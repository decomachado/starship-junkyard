const canvas = document.querySelector("canvas")
const c = canvas.getContext('2d')

canvas.width = innerWidth
canvas.height = innerHeight

const pi = Math.PI
const alertContainer = document.querySelector("#alert__container")
const alertMessage = alertContainer.querySelector('div')

const scoreDisplay = document.querySelector('#score')
const lifesRemainingDisplay = document.querySelector("#lifes_left")

const optionsContainer = document.querySelector('#options__container')

const backgroundMusic = document.querySelector('#background')
const shootingFx = document.querySelector('#shooting')
shootingFx.volume = "0.03"

const startGameBtn = document.querySelector('#start_game_button')

class Player {
    constructor() {
        this.position = {
            x: canvas.width / 2,
            y: canvas.height / 2
        }

        this.angle = 0
        this.speed = 5

        const image = new Image()
        image.src = "../images/game/player_spaceship.png"
        image.onload = () => {
            const scale = 1.4
            this.image = image
            this.width = image.width * scale
            this.height = image.height * scale
        }

        this.health = 5
    }

    draw(x = this.position.x, y = this.position.y, angle = this.angle) {
        if (this.image) {
            if (x < 0)
                x = 0
            else if (x > canvas.width)
                x = canvas.width

            if (y < 0)
                y = 0
            else if (y > canvas.height)
                y = canvas.height

            this.erase()

            c.save()
            c.translate(x, y)
            c.rotate(angle)
            c.drawImage(this.image, - this.width / 2, -this.height / 2, this.width, this.height)
            c.restore()
            
            this.position.x = x
            this.position.y = y
            this.angle = angle
        }
    }

    enemyHitIsAlive() {
        this.health -= 1
        return this.health > 0
    }

    erase() {
        c.save()
        c.translate(this.position.x, this.position.y)
        c.rotate(this.angle)
        c.clearRect(- this.width / 2 - 5, - this.height / 2 - 5, this.width + 10, this.height + 10)
        c.restore()
    }
}

class Projectile {
    constructor(position, angle) {

        this.position = {
            x: position.x,
            y: position.y
        }

        this.speed = 30
        this.angle = angle
        this.velocity = this.velocity()

        this.lastDrawn = {
            x: position.x,
            y: position.y
        }

        this.radius = 7
    }

    velocity() {
        let x, y
        if (this.angle >= pi && this.angle < pi * 3 / 2) {
            x = Math.cos((3 * pi / 2) - this.angle) * this.speed
            y = - Math.sin((3 * pi / 2) - this.angle) * this.speed
        } else if (this.angle >= pi * 3 / 2 && this.angle < pi * 2) {
            x = Math.cos(this.angle - (3 * pi / 2)) * this.speed
            y = Math.sin(this.angle - (3 * pi / 2)) * this.speed
        } else if (this.angle < pi / 2) {
            x = - Math.sin(this.angle) * this.speed
            y = Math.cos(this.angle) * this.speed
        } else {
            x = - Math.cos(this.angle - (pi / 2)) * this.speed
            y = - Math.sin(this.angle - (pi / 2)) * this.speed
        }

        return {
            x: x,
            y: y
        }
    }

    draw() {
        this.erase()

        c.save()
        c.translate(this.position.x, this.position.y)
        c.rotate(this.angle)
        c.beginPath()
        c.arc(0, 80, this.radius, 0, 2 * pi)
        c.fillStyle = "red"
        c.fill()
        c.restore()

        this.lastDrawn.x = this.position.x
        this.lastDrawn.y = this.position.y
    }

    update() {
        this.draw()
        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
    }

    erase() {
        c.save()
        c.translate(this.lastDrawn.x, this.lastDrawn.y)
        c.rotate(this.angle)
        c.clearRect(- this.radius - 1, - this.radius - 1 + 80, 16, 16)
        c.restore()
    }
}

class invaderProjectile {
    constructor(position, angle) {

        this.position = {
            x: position.x,
            y: position.y
        }

        this.speed = 4
        this.angle = angle
        this.velocity = this.velocity()

        this.lastDrawn = {
            x: position.x,
            y: position.y
        }

        this.radius = 13
    }

    velocity() {
        let x, y
        if (this.angle >= pi && this.angle < pi * 3 / 2) {
            x = Math.cos((3 * pi / 2) - this.angle) * this.speed
            y = - Math.sin((3 * pi / 2) - this.angle) * this.speed
        } else if (this.angle >= pi * 3 / 2 && this.angle < pi * 2) {
            x = Math.cos(this.angle - (3 * pi / 2)) * this.speed
            y = Math.sin(this.angle - (3 * pi / 2)) * this.speed
        } else if (this.angle < pi / 2) {
            x = - Math.sin(this.angle) * this.speed
            y = Math.cos(this.angle) * this.speed
        } else {
            x = - Math.cos(this.angle - (pi / 2)) * this.speed
            y = - Math.sin(this.angle - (pi / 2)) * this.speed
        }

        return {
            x: x,
            y: y
        }
    }

    draw() {
        this.erase()

        c.save()
        c.translate(this.position.x, this.position.y)
        c.rotate(this.angle)
        c.beginPath()
        c.arc(0, 40, this.radius, 0, 2 * pi)
        c.fillStyle = "green"
        c.fill()
        c.restore()

        this.lastDrawn.x = this.position.x
        this.lastDrawn.y = this.position.y
    }

    update() {
        this.draw()
        this.position.x += this.velocity.x
        this.position.y += this.velocity.y
    }

    erase() {
        c.save()
        c.translate(this.lastDrawn.x, this.lastDrawn.y)
        c.rotate(this.angle)
        c.clearRect(- this.radius - 1, - this.radius - 1 + 40, 28, 28)
        c.restore()
    }
}

class Invader {
    constructor(x, y, gridCoordinates) {
        this.position = {
            x: x,
            y: y
        }

        this.row = gridCoordinates.x
        this.column = gridCoordinates.y

        this.angle = 0
        this.speed = 50

        const image = new Image()
        image.src = "../images/game/invader_spaceship1.png"
        image.onload = () => {
            const scale = 1.4
            this.image = image
            this.width = image.width * scale
            this.height = image.height * scale
        }
    }

    draw(x = this.position.x, y = this.position.y, angle = this.angle) {
        if (this.image) {
            if (x < 0)
                x = 0
            else if (x > canvas.width)
                x = canvas.width

            if (y < 0)
                y = 0
            else if (y > canvas.height)
                y = canvas.height

            this.erase()

            c.save()
            c.translate(x, y)
            c.rotate(angle)
            c.drawImage(this.image, - this.width / 2, -this.height / 2, this.width, this.height)
            c.restore()
            
            this.position.x = x
            this.position.y = y
            this.angle = angle
        }
    }

    update(velocity, angle) {
        this.position.x += velocity.x
        this.position.y += velocity.y
        this.draw(...[,,], angle)
    }

    erase() {
        c.save()
        c.translate(this.position.x, this.position.y)
        c.rotate(this.angle)
        c.clearRect(- this.width / 2 - 5, - this.height / 2 - 5, this.width + 10, this.height + 10)
        c.restore()
    }

    shoot(invaderProjectiles) {
        invaderProjectiles.push(new invaderProjectile(this.position, this.angle))
    }
}

class grid {
    constructor(rws = null, clmns = null) {
        this.velocity = {
            x: 0,
            y: 0
        }

        this.speed = 2

        const temp = Math.round(Math.random() * 3)

        switch(temp) {
            case 0:
                this.velocity.x = this.speed
                this.velocity.y = this.speed
                break
            
            case 1: 
                this.velocity.x = - this.speed
                this.velocity.y = this.speed
                break

            case 2: 
                this.velocity.x = - this.speed
                this.velocity.y = - this.speed
                break
                
            case 3: 
                this.velocity.x = this.speed
                this.velocity.y = - this.speed
                break
        }

        this.invaders = []

        this.columns = clmns ? clmns : Math.round(Math.random() * 2) + gamelevel
        this.rows = rws ? rws : Math.round(Math.random() * 2) + gamelevel

        const offset = {
            x: Math.round(Math.random() * Math.round(canvas.width / 2)),
            y: Math.round(Math.random() * Math.round(canvas.height / 2))
        }

        for (let x = 0; x < this.columns; x++) {
            for (let y = 0; y < this.rows; y++){
                this.invaders.push(new Invader(x * 100 + offset.x, y * 100 + offset.y, {
                    x: y,
                    y: x
                }))
            }
        }

        this.position = {
            x: offset.x,
            y: offset.y
        }

        this.width = (this.columns - 1) * 100 
        this.height = (this.rows - 1) * 100

        this.update()
    } 

    update() {

        this.position.x += this.velocity.x
        this.position.y += this.velocity.y

        if (this.position.x + this.width >= canvas.width)
            this.velocity.x = - this.velocity.x
        else if (this.position.x <= 0)
            this.velocity.x = - this.velocity.x

        if (this.position.y + this.height >= canvas.height)
            this.velocity.y = - this.velocity.y
        else if (this.position.y <= 0)
            this.velocity.y = - this.velocity.y

        return this.velocity
    }

    nthRow(n) {
        return this.invaders.filter((invader) => invader.row === n)
    }

    nthColumn(n) {
        return this.invaders.filter((invader) => invader.column === n)
    }

    firstRow(row = 0) {
        const currentRow = this.nthRow(row)
        if (currentRow.length === 0) {
            return this.firstRow(row + 1)
        }
        return currentRow
    }

    lastRow(row = this.rows - 1) {
        const currentRow = this.nthRow(row)
        if (currentRow.length === 0) {
            return this.lastRow(row - 1)
        }
        return currentRow
    }

    firstColumn(column = 0) {
        const currentColumn = this.nthColumn(column)
        if (currentColumn.length === 0) {
            return this.firstColumn(column + 1)
        }
        return currentColumn
    }

    lastColumn(column = this.columns - 1) {
        const currentColumn = this.nthColumn(column)
        if (currentColumn.length === 0) {
            return this.lastColumn(column - 1)
        }
        return currentColumn
    }
}

function calculateUserRotation(x, y) {
    let theta

    if (x >= position.x && y < position.y) {
        theta = (3 * pi / 2) - Math.atan2(position.y - y, x - position.x)
    } else if (x > position.x && y >= position.y) {
        theta = (3 * pi / 2) + Math.atan2(y - position.y, x - position.x)
    } else if (x <= position.x && y > position.y) {
        theta = (pi / 2) - Math.atan2(y - position.y, position.x - x)
    } else {
        theta = (pi / 2) + Math.atan2(position.y - y, position.x - x)
    }

    position.angle = theta
}

function calculateInvaderRotation(x, y) {
    let theta

    if (position.x >= x && position.y < y) {
        theta = (3 * pi / 2) - Math.atan2(y - position.y, position.x - x)
    } else if (position.x > x && position.y >= y) { 
        theta = (3 * pi / 2) + Math.atan2(position.y - y, position.x - x)
    } else if (position.x <= x && position.y > y) {
        theta = (pi / 2) - Math.atan2(position.y - y, x - position.x)
    } else {
        theta = (pi / 2) + Math.atan2(y - position.y, x - position.x)
    }

    return theta
}

function calculateForwardness() {
    let x, y

    if(position.angle >= pi && position.angle < 3 * pi / 2){
        x = position.x + Math.cos((3 * pi / 2) - position.angle) * player.speed
        y = position.y - Math.sin((3 * pi / 2) - position.angle) * player.speed
    } else if (position.angle >= 3 * pi / 2 && position.angle < 2 * pi) {
        x = position.x + Math.cos(position.angle - (3 * pi / 2)) * player.speed
        y = position.y + Math.sin(position.angle - (3 * pi / 2)) * player.speed
    } else if (position.angle < pi / 2) {
        x = position.x - Math.sin(position.angle) * player.speed
        y = position.y + Math.cos(position.angle) * player.speed
    } else {
        x = position.x - Math.cos(position.angle - (pi / 2)) * player.speed
        y = position.y - Math.sin(position.angle - (pi / 2)) * player.speed
    }

    if (x < 0)
        x = 0
    else if (x > canvas.width)
        x = canvas.width

    if (y < 0)
        y = 0
    else if (y > canvas.height)
        y = canvas.height
    
    position.x = x
    position.y = y
}

function calculateBackwardness() {
    let x, y
    
    if(position.angle >= pi && position.angle < 3 * pi / 2){
        x = position.x - Math.cos((3 * pi / 2) - position.angle) * player.speed
        y = position.y + Math.sin((3 * pi / 2) - position.angle) * player.speed
    } else if (position.angle >= 3 * pi / 2 && position.angle < 2 * pi) {
        x = position.x - Math.cos(position.angle - (3 * pi / 2)) * player.speed
        y = position.y - Math.sin(position.angle - (3 * pi / 2)) * player.speed
    } else if (position.angle < pi / 2) {
        x = position.x + Math.sin(position.angle) * player.speed
        y = position.y - Math.cos(position.angle) * player.speed
    } else {
        x = position.x + Math.cos(position.angle - (pi / 2)) * player.speed
        y = position.y + Math.sin(position.angle - (pi / 2)) * player.speed
    }

    if (x < 0)
        x = 0
    else if (x > canvas.width)
        x = canvas.width

    if (y < 0)
        y = 0
    else if (y > canvas.height)
        y = canvas.height

    position.x = x
    position.y = y
}

function restartGame() {
   player.erase()

    projectiles.forEach(projectile => {
        projectile.erase()
    })

    invaderProjectiles.forEach(invaderProjectile => {
        invaderProjectile.erase()
    })

    grids.forEach(grid => {
        grid.invaders.forEach(invader => {
            invader.erase()
        })
    })

    gameLost = false
    gameState = false
    score = 0
    frames = 0
    gamelevel = 0

    position = {
        x: canvas.width / 2,
        y: canvas.height / 2,
        angle: 0
    }

    projectiles = []
    invaderProjectiles = []
    grids = []
    player = new Player()

    alertContainer.style.display = "none"
    alertMessage.innerHTML = ''

    setTimeout(() => {
        animate()
    }, 200)
    
}

function gameOver() {
    clearTimeout(gameOverTimeout)

    gameOverTimeout = setTimeout(() => {
        cancelAnimationFrame(itsGameTime)
        alertContainer.style.display = "flex"
        gameLost = true
    
        const h1 = document.createElement('h1')
        h1.innerHTML = "Game Over!"
    
        const p = document.createElement('p')
        p.innerHTML = "Reload page to start again"
    
        const p2 = document.createElement('p')
        p2.innerHTML = "or"
    
        const button = document.createElement('button')
        button.id = "restart_game"
        button.innerHTML = "click here"
    
        button.addEventListener('click', restartGame)
    
        alertMessage.appendChild(h1)
        alertMessage.appendChild(p)
        alertMessage.appendChild(p2)
        alertMessage.appendChild(button)
    }, 20)
}

let itsGameTime
let gameState = false
let gameLost = false
let gamelevel = 0 
let gameStarted = false
let gameOverTimeout

let soundFx = false

let player = new Player()
let grids = []
let projectiles = []
let invaderProjectiles = []
let score = 0

scoreDisplay.innerHTML = `score: ${score}`
lifesRemainingDisplay.innerHTML = `lifes remaining: ${player.health}`

let frames = 0 
let randomInterval = Math.round(Math.random() * 500) + 500
let position = {
    x: canvas.width / 2,
    y: canvas.height / 2,
    angle: 0
}

let keydown = null

// <-------------------------------------------->
// Game loop

function animate() {
    if (!gameLost) {
        gameState = true
    
        player.draw(position.x, position.y, position.angle)
    
        invaderProjectiles.forEach((invaderProjectile, index) => {
            if (invaderProjectile.position.x < 0 || invaderProjectile.position.x > canvas.width || invaderProjectile.position.y < 0 || invaderProjectile.position.y > canvas.height) {
                setTimeout(() => {
                    invaderProjectiles.splice(index, 1)
                }, 0)
            } else {
                invaderProjectile.update()
            }
    
            if (invaderProjectile.position.x + invaderProjectile.radius >= player.position.x - player.width / 2 && invaderProjectile.position.x - invaderProjectile.radius <= player.position.x + player.width / 2 && invaderProjectile.position.y + invaderProjectile.radius >= player.position.y - player.height / 2 && 
            invaderProjectile.position.y - invaderProjectile.radius <= player.position.y + player.height / 2) {
    
                invaderProjectile.erase()
                invaderProjectiles.splice(index, 1)
    
                if (!player.enemyHitIsAlive()) {
                    gameOver()
                }
            }
        })
    
        if (projectiles) {
            projectiles.forEach((projectile, index) => {
                if (projectile.lastDrawn.x < 0 || projectile.lastDrawn.x > canvas.width || projectile.lastDrawn.y < 0 || projectile.lastDrawn.y > canvas.height){
                    setTimeout(() => {
                        projectiles.splice(index, 1)
                    }, 0)
                } else
                    projectile.update()
            })
        }
    
        grids.forEach((grid, gridIndex) => {
            const velocity = grid.update()
    
            if (frames % 200 === 0 && grid.invaders.length > 0) {
                grid.invaders[Math.floor(Math.random() * grid.invaders.length)].shoot(invaderProjectiles)
            }
            
            grid.invaders.forEach((invader, i) => {
                invader.update(velocity, calculateInvaderRotation(invader.position.x, invader.position.y))

                if(invader.position.x + invader.width / 2 >= player.position.x - player.width / 2 &&
                invader.position.x - invader.width / 2 <= player.position.x + player.width / 2 && 
                invader.position.y + invader.height / 2 >= player.position.y - player.height / 2 &&
                invader.position.y - invader.height / 2 <= player.position.y + player.height / 2) {
                    if(player.enemyHitIsAlive()) {
                        invader.erase()
                        setTimeout(() => {
                            grid.invaders.splice(i, 1)
                        }, 0)
                    } else {
                        gameOver()
                    }
                }
    
                projectiles.forEach((projectile, j) => {
                    if (projectile.position.x + projectile.radius >= invader.position.x - invader.width / 2 &&
                    projectile.position.x - projectile.radius <= invader.position.x + invader.width / 2 && 
                    projectile.position.y + projectile.radius >= invader.position.y - invader.height / 2 && 
                    projectile.position.y - projectile.radius <= invader.position.y + invader.height / 2) {
                        setTimeout(() => {
                            const invaderFound = grid.invaders.find(invader2 => invader2 === invader)
                            const projectileFound = projectiles.find(projectile2 => projectile2 === projectile)
    
                            if (invaderFound && projectileFound){
                                invader.erase()
                                projectile.erase()
                                grid.invaders.splice(i, 1)
                                projectiles.splice(j, 1)
                                score += 10
                            }
                            
                            if (grid.invaders.length > 0){
                                const topInvader = grid.firstRow()[0]
                                const bottomInvader = grid.lastRow()[0]
                                const leftInvader = grid.firstColumn()[0]
                                const rightInvader = grid.lastColumn()[0]
    
                                grid.position.x = leftInvader.position.x
                                grid.width = rightInvader.position.x - leftInvader.position.x
    
                                grid.position.y = topInvader.position.y
                                grid.height = bottomInvader.position.y - topInvader.position.y
                            } else {
                                grids.splice(gridIndex, 1)
                            }
                        }, 0)
                    }
                })
            })
        })
    
        if (keydown == "KeyW")
            calculateForwardness()
        if (keydown == "KeyS")
            calculateBackwardness()
    
        if (frames % randomInterval === 0) {
            grids.push(new grid())
            randomInterval = Math.round(Math.random() * 500) + 500
        }

        if (frames % 3000 === 0) {
            gamelevel += 1
        }
        
        frames += 1

        scoreDisplay.innerHTML = `score: ${score}`
        lifesRemainingDisplay.innerHTML = `lifes remaining: ${player.health >= 0 ? player.health : 0}`
    
        itsGameTime = requestAnimationFrame(animate) 
    }
}

// <-------------------------------------------->
// Event Listeners

startGameBtn.addEventListener('click', () => {
    if (!gameStarted) {
        if (document.forms[0].elements[0].checked) {
            backgroundMusic.volume = "0.02"
            backgroundMusic.play()
        }

        if (document.forms[0].elements[1].checked) {
            soundFx = true
        }

        optionsContainer.style.display = "none"
        
        setTimeout(() => {
            gameStarted = true
            animate()
        }, 200)
    }
})

addEventListener('resize', function() {
    canvas.width = innerWidth
    canvas.height = innerHeight
})

addEventListener('mousemove', (e) => {
    if (gameState && gameStarted)
        calculateUserRotation(e.offsetX, e.offsetY)
})

addEventListener('keydown', (e) => {
    if (['KeyW', 'KeyS'].indexOf(e.code) > -1 && gameStarted)
        keydown = e.code
})

addEventListener('keyup', (e) => {
    if (['KeyW', 'KeyS'].indexOf(e.code) > -1)
        keydown = null
})

addEventListener('click', (e) => {
    if(gameState && gameStarted && !gameLost){
        projectiles.push(new Projectile(position, position.angle))

        // console.log(frames) 
        // setTimeout(() => {
        //     console.log(frames)
        // }, 1000)

        if (soundFx) {
            shootingFx.play()
        }
    }
})

addEventListener('keyup', (e) => {
    if (gameStarted) {
        if (e.code == "Escape" && gameState && !gameLost) {
            cancelAnimationFrame(itsGameTime)
            gameState = false
    
            
            alertContainer.style.display = "flex"
    
            const h1 = document.createElement('h1')
            h1.innerHTML = "Game Paused!"
    
            const p = document.createElement('p')
            p.innerHTML = "Press Escape to return to game"
    
            alertMessage.appendChild(h1)
            alertMessage.appendChild(p)
            
        } else if (e.code == "Escape" && !gameState && !gameLost) {
            alertContainer.style.display = "none"
            alertMessage.innerHTML = ''
    
            itsGameTime = requestAnimationFrame(animate)
        }
    }
})